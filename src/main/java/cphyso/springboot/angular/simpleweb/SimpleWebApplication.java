package cphyso.springboot.angular.simpleweb;


import cphyso.springboot.angular.simpleweb.model.Employee;
import cphyso.springboot.angular.simpleweb.repository.EmployeeRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import java.util.stream.Stream;

@SpringBootApplication
public class SimpleWebApplication {

    public static void main(String[] args) {
        SpringApplication.run(SimpleWebApplication.class, args);
    }

    @Bean
    CommandLineRunner init(EmployeeRepository employeeRepository) {

        return args -> {

            Stream.of("dan","don","den","din","dun").forEach(name -> {
                Employee emp = new Employee(name, name.toLowerCase() + "@work.com");
                employeeRepository.save(emp);
            });
            employeeRepository.findAll().forEach(System.out::println);

            System.out.println("********************************************************************************************************************************************************************************************************************");

//            LOGGER.log(Level.INFO,"Adding employees....\n");
//            employeeController.addEmployee(new Employee("Hank", "Hill", "dev","Salesman", 63000.00));
//            employeeController.addEmployee(new Employee("Peggy", "Hill", "Substitute", "dev",43000.00));
//            employeeController.addEmployee(new Employee("Homer", "Simpson", "Plant Operator", "dev",163000.00));
//            employeeController.addEmployee(new Employee("Rick", "Sanchez", "Scientis", "dev",633000.00));
//            employeeController.addEmployee(new Employee("Bob", "Belcher", "Fry Cook", "dev",10.00));
//            LOGGER.log(Level.INFO,"Getting all employees....");
//            employeeController.getEmployees().forEach(System.out::println);


//            System.out.println("\nGetting employee with id = 1....");
//            System.out.println(employeeService.getEmployee(1L));
//
//            System.out.println("\nUpdating employee with id = 1....");
//            System.out.println("Before----->" + employeeService.getEmployee(1L));
//            Employee employee = employeeService.getEmployee(1L);
//            employee.setFirstName("Marge");
//            System.out.println("After------>" + employeeService.updateEmployee(employee));
//
//            System.out.println("\nDeleting employee....");
//            employeeService.deleteEmployee(1L);
        };
    }
}
