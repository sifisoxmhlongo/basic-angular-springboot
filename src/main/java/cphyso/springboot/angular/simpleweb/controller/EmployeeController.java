package cphyso.springboot.angular.simpleweb.controller;

import cphyso.springboot.angular.simpleweb.model.Employee;
import cphyso.springboot.angular.simpleweb.repository.EmployeeRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class EmployeeController {

    
    private final EmployeeRepository employeeRepository;

    public EmployeeController(EmployeeRepository employeeRepository){
        this.employeeRepository = employeeRepository;
    }

    @GetMapping("/employees")
    public List<Employee> getEmployees(){
        return (List<Employee>) employeeRepository.findAll();
    }

    @PostMapping("/employees")
    public void addEmployee(@RequestBody Employee emp){
        employeeRepository.save(emp);
    }
}
